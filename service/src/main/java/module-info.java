module com.example.socket {
    exports com.example.socket;
    exports com.example.socket.spi;
    uses com.example.socket.spi.NetworkSocketProvider;
}