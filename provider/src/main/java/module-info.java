module com.example.fastsocket {
    requires com.example.socket;
    provides com.example.socket.spi.NetworkSocketProvider
            with com.example.fastsocket.FastNetworkSocketProvider;
}